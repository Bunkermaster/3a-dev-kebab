<?php

namespace App\Service;

class AddLeService
{
    public function addLe(string $value): string
    {
        return "Le ".$value;
    }
}
