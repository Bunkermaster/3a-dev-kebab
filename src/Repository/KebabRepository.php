<?php

namespace App\Repository;

use App\Entity\Kebab;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Kebab|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kebab|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kebab[]    findAll()
 * @method Kebab[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KebabRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Kebab::class);
    }

    // /**
    //  * @return Kebab[] Returns an array of Kebab objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Kebab
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
