<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200416111059 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ingredient (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kebab_ingredient (kebab_id INT NOT NULL, ingredient_id INT NOT NULL, INDEX IDX_E744952BD86CD3C0 (kebab_id), INDEX IDX_E744952B933FE08C (ingredient_id), PRIMARY KEY(kebab_id, ingredient_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE kebab_ingredient ADD CONSTRAINT FK_E744952BD86CD3C0 FOREIGN KEY (kebab_id) REFERENCES kebab (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kebab_ingredient ADD CONSTRAINT FK_E744952B933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kebab_ingredient DROP FOREIGN KEY FK_E744952B933FE08C');
        $this->addSql('DROP TABLE ingredient');
        $this->addSql('DROP TABLE kebab_ingredient');
    }
}
