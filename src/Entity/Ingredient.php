<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IngredientRepository")
 */
class Ingredient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Kebab", mappedBy="ingredients")
     */
    private $kebabs;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->kebabs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Kebab[]
     */
    public function getKebabs(): Collection
    {
        return $this->kebabs;
    }

    public function addKebab(Kebab $kebab): self
    {
        if (!$this->kebabs->contains($kebab)) {
            $this->kebabs[] = $kebab;
            $kebab->addIngredient($this);
        }

        return $this;
    }

    public function removeKebab(Kebab $kebab): self
    {
        if ($this->kebabs->contains($kebab)) {
            $this->kebabs->removeElement($kebab);
            $kebab->removeIngredient($this);
        }

        return $this;
    }
}
